#include <sourcemod>
#include <sdkhooks>
#include <sdkhooks>
#include <cstrike>
#include <csgocolors>
#include <ranking>

#undef REQUIRE_PLUGIN

#include <chat-processor>

#define PLUGIN_VERSION "1.0"

public Plugin myinfo = {
	name = "Ranking - Chattags",
	author = "Zipcore",
	description = "Extends the Ranking plugin with chattrag by rank",
	version = PLUGIN_VERSION,
	url = "www.zipcore.net",
};

KeyValues g_kvRanks;

int g_iRankIndex[MAXPLAYERS + 1];

char g_sUnranked[256];
char g_sClanTagUnranked[256];
char g_sChatTagUnranked[256];
char g_sNameColorUnranked[256];
char g_sChatColorUnranked[256];

char g_sRank[MAXPLAYERS + 1][256];
char g_sClanTag[MAXPLAYERS + 1][256];
char g_sChatTag[MAXPLAYERS + 1][256];
char g_sNameColor[MAXPLAYERS + 1][256];
char g_sChatColor[MAXPLAYERS + 1][256];

public void OnPluginStart() 
{
	LoadConfig();
	
	HookEvent("player_team", Event_OnPlayerTeam_Post, EventHookMode_Post);
	
	RegConsoleCmd("sm_tags", Cmd_ListTags);
	RegConsoleCmd("sm_ranks", Cmd_ListTags);
}

public Action CP_OnChatMessage(int& author, ArrayList recipients, char[] flagstring, char[] name, char[] message, bool& processcolors, bool& removecolors)
{
	if(author <= 0)
		return Plugin_Continue;
	
	if(strlen(g_sChatTag[author]))
		Format(name, MAXLENGTH_NAME, " {default}[%s{default}]%s%s%s", g_sChatTag[author], strlen(g_sChatTag[author]) ? " " : "", g_sNameColor[author], name);
	else Format(name, MAXLENGTH_NAME, " %s%s", g_sNameColor[author], name);
	Format(message, MAXLENGTH_MESSAGE, "%s%s", g_sChatColor[author], message);
	
	return Plugin_Changed;
}

public Action Event_OnPlayerTeam_Post(Handle event, const char[] name, bool dontBroadcast)
{
	int iClient = GetClientOfUserId(GetEventInt(event, "userid"));
	CS_SetClientClanTag(iClient, g_sClanTag[iClient]);
	return Plugin_Continue;
}

public void OnClientPostAdminCheck(int iClient) 
{
	OnClientDisconnect_Post(iClient);
	CS_SetClientClanTag(iClient, g_sClanTag[iClient]);
}

public OnClientDisconnect_Post(int iClient)
{
	g_sRank[iClient] = g_sUnranked;
	g_sClanTag[iClient] = g_sClanTagUnranked;
	g_sChatTag[iClient] = g_sChatTagUnranked;
	g_sNameColor[iClient] = g_sNameColorUnranked;
	g_sChatColor[iClient] = g_sChatColorUnranked;
}

void LoadConfig()
{
	g_kvRanks = CreateKeyValues("Rankings");
	
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/ranking/ranks.cfg");
	
	FileToKeyValues(g_kvRanks, sPath);
}

public void Ranking_OnRankLoaded(int iClient)
{
	int iRank = Ranking_GetRank(iClient);
	if(iRank < 1)
		return;
	
	float fPos = float(iRank) / float(Ranking_GetTotal()) * 100.0;
	
	if(iRank == 1)
		fPos = 0.0;
	
	int iIndex;
	g_kvRanks.GotoFirstSubKey(false);
	do 
	{
		iIndex++;
		
		if(g_kvRanks.GetFloat("default") == 1)
		{
			g_kvRanks.GetSectionName(g_sUnranked, sizeof(g_sUnranked));
			g_kvRanks.GetString("clantag", g_sClanTagUnranked, sizeof(g_sClanTagUnranked));
			g_kvRanks.GetString("chattag", g_sChatTagUnranked, sizeof(g_sChatTagUnranked));
			g_kvRanks.GetString("namecolor", g_sNameColorUnranked, sizeof(g_sNameColorUnranked));
			g_kvRanks.GetString("chatcolor", g_sChatColorUnranked, sizeof(g_sChatColorUnranked));
		}
		else if(g_kvRanks.GetFloat("perc", -1.0) >= fPos)
		{
			char sRank[256], sClanTag[256], sChatTag[256], sNameColor[256], sChatColor[256];
			
			g_kvRanks.GetSectionName(sRank, sizeof(sRank));
			g_kvRanks.GetString("clantag", sClanTag, sizeof(sClanTag));
			g_kvRanks.GetString("chattag", sChatTag, sizeof(sChatTag));
			g_kvRanks.GetString("namecolor", sNameColor, sizeof(sNameColor));
			g_kvRanks.GetString("chatcolor", sChatColor, sizeof(sChatColor));
			
			g_iRankIndex[iClient] = iIndex;
			
			g_sRank[iClient] = sRank;
			g_sClanTag[iClient] = sClanTag;
			g_sChatTag[iClient] = sChatTag;
			g_sNameColor[iClient] = sNameColor;
			g_sChatColor[iClient] = sChatColor;
			
			CS_SetClientClanTag(iClient, g_sClanTag[iClient]);
			
			break;
		}
		
	} while (g_kvRanks.GotoNextKey(false));
	g_kvRanks.Rewind();
}

public Action Cmd_ListTags(int iClient, int iArgs)
{
	Menu_ListTags(iClient);
	return Plugin_Handled;
}

void Menu_ListTags(int iClient)
{
	Menu menu = new Menu(MenuHandler_ListTags);
	menu.SetTitle("Chattags:");
	
	char sBuffer[32], sInfo[16], sRank[256];
	int iRank, iRank2;
	float fTotal = float(Ranking_GetTotal());
	
	int iIndex;
	g_kvRanks.GotoFirstSubKey(false);
	do 
	{
		iIndex++;
		
		float fPerc = g_kvRanks.GetFloat("perc", -1.0);
		
		if(g_kvRanks.GetFloat("default") == 1)
			continue;
			
		if(fPerc == 0.0)
		{
			iRank = 1;
		}
		else
		{
			iRank = iRank2 + 1;
			iRank2 = RoundToFloor(fTotal * fPerc);
		}
		
		
		g_kvRanks.GetSectionName(sRank, sizeof(sRank));
		
		IntToString(iIndex, sInfo, sizeof(sInfo));
		Format(sBuffer, sizeof(sBuffer), "%s", sRank);
		
		// Current Tag
		if(g_iRankIndex[iClient] == iIndex)
			Format(sBuffer, sizeof(sBuffer), "%s [✓]", sBuffer);
		
		if(fPerc == 0.0)
			Format(sBuffer, sizeof(sBuffer), "%s\nTop 1", sBuffer);
		else Format(sBuffer, sizeof(sBuffer), "%s\nTop %i - %i", sBuffer, iRank, iRank2);
		
		menu.AddItem(sInfo, sBuffer);
	} while (g_kvRanks.GotoNextKey(false));
	g_kvRanks.Rewind();
	
	SetMenuExitButton(menu, true);
	menu.Display(iClient, MENU_TIME_FOREVER);
}

public int MenuHandler_ListTags(Menu menu, MenuAction action, int client, int params)
{
	if (action == MenuAction_Select)
	{
		char sInfo[64];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		
	}
	else if (action == MenuAction_End)
		delete menu;
}