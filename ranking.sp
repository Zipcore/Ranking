#include <sourcemod>
#include <sdkhooks>
#include <sdkhooks>
#include <cstrike>
#include <csgocolors>
#include <ranking>

#undef REQUIRE_PLUGIN

#include <chat-processor>

#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && !IsFakeClient(%1))

#define LoopArray(%1,%2) for(int %1=0;%1<GetArraySize(%2);++%1)

Database g_dMain = null;
int g_iTotal;

Handle g_hTimer = null;

Handle g_hAuthTimer[MAXPLAYERS + 1] = { null, ... };
char g_sAuth[MAXPLAYERS + 1][18];
int g_iPoints[MAXPLAYERS + 1] =  { -1, ... };
int g_iPointsRank[MAXPLAYERS + 1] =  { -1, ... };

Handle g_OnRankLoaded;

#include "ranking/ConnectSQL.sp"
#include "ranking/LoadPlayer.sp"
#include "ranking/UpdatePlayer.sp"
#include "ranking/Top.sp"
#include "ranking/RankMe.sp"

#define PLUGIN_VERSION "1.0"

public Plugin myinfo = {
	name = "Ranking",
	author = "Zipcore",
	description = "Ranking system",
	version = PLUGIN_VERSION,
	url = "www.zipcore.net",
};

public APLRes AskPluginLoad2(Handle hMyself, bool bLate, char[] sError, int iMaxError) 
{
	RegPluginLibrary("ranking");
	g_OnRankLoaded = CreateGlobalForward("Ranking_OnRankLoaded", ET_Ignore, Param_Cell);
	
	CreateNative("Ranking_GetTotal", Native_GetTotal);
	CreateNative("Ranking_GetPoints", Native_GetPoints);
	CreateNative("Ranking_GetRank", Native_GetRank);
	CreateNative("Ranking_AddPoints", Native_AddPoints);
	CreateNative("Ranking_TakePoints", Native_TakePoints);
	
	InitToplist();
	
	Ranking_MarkNativesAsOptional();
	__pl_chat_processor_SetNTVOptional();
	
	return APLRes_Success;
}

public void OnPluginStart() 
{
	ConnectDB();
	
	RegConsoleCmd("sm_rank", Cmd_Rank);
	RegConsoleCmd("sm_top10", Cmd_Top);
}

public void OnClientPostAdminCheck(int iClient) 
{
	OnClientDisconnect_Post(iClient);
	LoadPlayer(iClient);
}

public OnClientDisconnect_Post(int iClient)
{
	g_sAuth[iClient] = "";
	g_iPoints[iClient] = -1;
	g_iPointsRank[iClient] = -1;
	
	if(g_hAuthTimer[iClient] != null)
	{
		delete g_hAuthTimer[iClient];
		g_hAuthTimer[iClient] = null;
	}
}

public int Native_GetTotal(Handle hPlugin, int iParams) 
{
	return g_iTotal;
}

public int Native_GetPoints(Handle hPlugin, int iParams) 
{
	return g_iPoints[GetNativeCell(1)];
}

public int Native_GetRank(Handle hPlugin, int iParams) 
{
	return g_iPointsRank[GetNativeCell(1)];
}

public int Native_AddPoints(Handle hPlugin, int iParams) 
{
	return AddPoints(GetNativeCell(1), GetNativeCell(2));
}

public int Native_TakePoints(Handle hPlugin, int iParams) 
{
	return TakePoints(GetNativeCell(1), GetNativeCell(2));
}