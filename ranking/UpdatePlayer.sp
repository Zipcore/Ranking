bool AddPoints(int iClient, int iPoints) 
{
	if(iClient <= 0 || !IsClientInGame(iClient) || g_dMain == null || g_iPoints[iClient] == -1 || iPoints <= 0)
		return false;
	
	char sBuffer[512];
	FormatEx(sBuffer, sizeof(sBuffer), "UPDATE `ranking` SET `points`=`points`+%d WHERE steamid = '%s';", iPoints, g_sAuth[iClient]);
	
	DataPack pack = new DataPack();
	g_dMain.Query(Callback_AddPoints, sBuffer, pack, DBPrio_High);
	
	pack.WriteCell(GetClientUserId(iClient));
	pack.WriteCell(iPoints);
	
	return true;
}

bool TakePoints(int iClient, int iPoints) 
{
	if(iClient <= 0 || !IsClientInGame(iClient) || g_dMain == null || g_iPoints[iClient] == -1 || iPoints <= 0)
		return false;
	if(g_iPoints[iClient] - iPoints < 1)
		iPoints = g_iPoints[iClient] - 1;
	
	char sBuffer[512];
	FormatEx(sBuffer, sizeof(sBuffer), "UPDATE `ranking` SET `points`=`points`-%d WHERE steamid = '%s';", iPoints, g_sAuth[iClient]);
	
	DataPack pack = new DataPack();
	g_dMain.Query(Callback_TakePoints, sBuffer, pack, DBPrio_High);
	
	pack.WriteCell(GetClientUserId(iClient));
	pack.WriteCell(iPoints);
	
	return true;
}

public void Callback_AddPoints(Database db, DBResultSet results, const char[] error, Handle pack)
{
	ResetPack(pack);
	
	int iClient = GetClientOfUserId(ReadPackCell(pack));
	int iPoints = ReadPackCell(pack);
	delete pack;
	
	if (db == null) 
	{
		LogError("(Callback_UpdatePlayer) - %s", error);
		return;
	}
	
	if(iClient <= 0 || !IsClientInGame(iClient))
		return;
	
	g_iPoints[iClient] += iPoints;
	
	CPrintToChat(iClient, "{lime}You got {yellow}%i point%s{lime}. New score: {yellow}%i point%s{lime}.", iPoints, iPoints == 1 ? "" : "s", g_iPoints[iClient], g_iPoints[iClient] == 1 ? "" : "s");
}

public void Callback_TakePoints(Database db, DBResultSet results, const char[] error, Handle pack)
{
	ResetPack(pack);
	
	int iClient = GetClientOfUserId(ReadPackCell(pack));
	int iPoints = ReadPackCell(pack);
	delete pack;
	
	if (db == null) 
	{
		LogError("(Callback_UpdatePlayer) - %s", error);
		return;
	}
	
	if(iClient <= 0 || !IsClientInGame(iClient))
		return;
	
	g_iPoints[iClient] -= iPoints;
	
	CPrintToChat(iClient, "{lime}You lost {yellow}%i point%s{lime}. New score: {yellow}%i point%s{lime}.", iPoints, iPoints == 1 ? "" : "s", g_iPoints[iClient], g_iPoints[iClient] == 1 ? "" : "s");
}