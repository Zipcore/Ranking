public Action Cmd_Rank(int iClient, int iArgs)
{
	RankeMe(iClient);
	
	return Plugin_Handled;
}

void RankeMe(int iClient)
{
	CPrintToChat(iClient, "{yellow}[Ranking] {lime}You are currently ranked {darkred}%i{lime} out of {darkred}%i{lime} with a total of {darkred}%i{lime} points.", g_iPointsRank[iClient], g_iTotal, g_iPoints[iClient]);
}