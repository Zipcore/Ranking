

public void ConnectDB() 
{
	if(!SQL_CheckConfig("ranking"))
		SetFailState("Invalid or couldn't find database configuration for \"ranking\" inside \"addons/sourcemod/configs/databases.cfg\"");
	
	Database.Connect(Callback_ConnectDB, "ranking");
}

public void Callback_ConnectDB(Database db, const char[] error, int data)
{
	if (db == null) 
	{
		SetFailState("(ConnectDB-Ranking) - %s", error);
		return;
	}
	
	g_dMain = db;
	g_dMain.SetCharset("utf8mb4");
	
	CreateTable();
}

void CreateTable()
{
	char sQuery[2048];
	FormatEx(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS `ranking` (steamid VARCHAR(17) NOT NULL, points INT NOT NULL, name VARCHAR(64) NOT NULL, PRIMARY KEY (`steamid`), KEY (steamid));");
	g_dMain.Query(Callback_CreateTable, sQuery, _, DBPrio_High);
}

public void Callback_CreateTable(Database db, DBResultSet results, const char[] error, int data)
{
	if (db == null) 
	{
		LogError("(CreateTable) - %s", error);
		return;
	}
	
	GetTotal(true);
	StartTimer();
}

void GetTotal(bool late = false)
{
	g_dMain.Query(Callback_LoadTotal, "SELECT COUNT(*) FROM `ranking` WHERE 1", late);
}

public void Callback_LoadTotal(Handle hOwner, Handle hHndl, char[] sError, int data) 
{
	if (hHndl == null) 
	{
		LogError("(LoadTotal) - %s", sError);
		return;
	}
	
	if(SQL_FetchRow(hHndl))
		g_iTotal = SQL_FetchInt(hHndl, 0);
	
	if(data)
	{
		LoopIngamePlayers(iClient)
			OnClientPostAdminCheck(iClient);
	}
}