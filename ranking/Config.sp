void LoadConfig()
{
	g_kvRanks = CreateKeyValues("Rankings");
	
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/ranking/ranks.cfg");
	
	FileToKeyValues(g_kvRanks, sPath);
}

void GetRank(int iClient)
{
	if(g_iPointsRank[iClient] == -1)
		return;
	
	float fPos = float(g_iPointsRank[iClient]) / float(g_iTotal) * 100.0;
	
	if(g_iPointsRank[iClient] == 1)
		fPos = 0.0;
	
	int iIndex;
	g_kvRanks.GotoFirstSubKey(false);
	do 
	{
		iIndex++;
		
		if(g_kvRanks.GetFloat("default") == 1)
		{
			g_kvRanks.GetSectionName(g_sUnranked, sizeof(g_sUnranked));
			g_kvRanks.GetString("clantag", g_sClanTagUnranked, sizeof(g_sClanTagUnranked));
			g_kvRanks.GetString("chattag", g_sChatTagUnranked, sizeof(g_sChatTagUnranked));
			g_kvRanks.GetString("namecolor", g_sNameColorUnranked, sizeof(g_sNameColorUnranked));
			g_kvRanks.GetString("chatcolor", g_sChatColorUnranked, sizeof(g_sChatColorUnranked));
		}
		else if(g_kvRanks.GetFloat("perc", -1.0) >= fPos)
		{
			char sRank[256], sClanTag[256], sChatTag[256], sNameColor[256], sChatColor[256];
			
			g_kvRanks.GetSectionName(sRank, sizeof(sRank));
			g_kvRanks.GetString("clantag", sClanTag, sizeof(sClanTag));
			g_kvRanks.GetString("chattag", sChatTag, sizeof(sChatTag));
			g_kvRanks.GetString("namecolor", sNameColor, sizeof(sNameColor));
			g_kvRanks.GetString("chatcolor", sChatColor, sizeof(sChatColor));
			
			g_iRankIndex[iClient] = iIndex;
			
			g_sRank[iClient] = sRank;
			g_sClanTag[iClient] = sClanTag;
			g_sChatTag[iClient] = sChatTag;
			g_sNameColor[iClient] = sNameColor;
			g_sChatColor[iClient] = sChatColor;
			
			CS_SetClientClanTag(iClient, g_sClanTag[iClient]);
			
			break;
		}
		
	} while (g_kvRanks.GotoNextKey(false));
	g_kvRanks.Rewind();
}