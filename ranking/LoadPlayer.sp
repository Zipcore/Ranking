public Action Timer_PostAdminCheck(Handle hTimer, int iClient) 
{
	if(g_hAuthTimer[iClient] != hTimer)
	{
		LogMessage("There is another auth timer already running.");
		g_hAuthTimer[iClient] = null;
		return Plugin_Handled;
	}
	
	g_hAuthTimer[iClient] = null;

	LoadPlayer(iClient);

	return Plugin_Handled;
}

void LoadPlayer(int iClient) 
{
	if(iClient <= 0 || !IsClientInGame(iClient))
		return;
	
	if(!GetClientAuthId(iClient, AuthId_SteamID64, g_sAuth[iClient], 18) || g_dMain == null)
	{
		g_hAuthTimer[iClient] = CreateTimer(1.5+0.025*float(iClient), Timer_PostAdminCheck, iClient, TIMER_FLAG_NO_MAPCHANGE);
		return;
	}
	
	char sBuffer[512];
	FormatEx(sBuffer, sizeof(sBuffer), "SELECT points FROM `ranking` WHERE steamid = '%s';", g_sAuth[iClient]);
	g_dMain.Query(Callback_LoadPlayer, sBuffer, GetClientUserId(iClient), DBPrio_High);
}

public void Callback_LoadPlayer(Database db, DBResultSet results, const char[] error, int data)
{
	if (db == null) 
	{
		LogError("(LoadPlayer) - %s", error);
		return;
	}
	
	int iClient = GetClientOfUserId(data);
	
	if(iClient <= 0 || !IsClientInGame(iClient))
	{
		OnClientDisconnect_Post(iClient);
		return;
	}
		
	if(!GetClientAuthId(iClient, AuthId_SteamID64, g_sAuth[iClient], 18))
	{
		g_hAuthTimer[iClient] = CreateTimer(1.0, Timer_PostAdminCheck, GetClientUserId(iClient), TIMER_FLAG_NO_MAPCHANGE);
		return;
	}
	
	if (results.FetchRow()) 
	{
		g_iPoints[iClient] = results.FetchInt(0);
		
		LoadRank(iClient);
		
		char sName[64];
		GetClientName(iClient, sName, sizeof(sName));
		
		char sSafeName[129];
		g_dMain.Escape(sName, sSafeName, sizeof(sSafeName));
		
		char sQuery[512];
		Format(sQuery, sizeof(sQuery), "UPDATE `ranking` SET `name`= '%s' WHERE steamid = '%s';", sSafeName, g_sAuth[iClient]);
		g_dMain.Query(Callback_UpdateName, sQuery, _, DBPrio_Low);
		return;
	}
	
	InsertUser(iClient);
}

public void Callback_UpdateName(Database db, DBResultSet results, const char[] error, int data)
{
	if (db == null) 
	{
		LogError("(UpdateName) - %s", error);
		return;
	}
}

void InsertUser(int iClient)
{
	if(!GetClientAuthId(iClient, AuthId_SteamID64, g_sAuth[iClient], 18))
		return;
	
	char sName[64];
	GetClientName(iClient, sName, sizeof(sName));
	
	char sSafeName[129];
	g_dMain.Escape(sName, sSafeName, sizeof(sSafeName));
	
	char sBuffer[512];
	FormatEx(sBuffer, sizeof(sBuffer), "INSERT INTO `ranking` (steamid, points, name) VALUES ('%s', 0, '%s');", g_sAuth[iClient], sSafeName);
	g_dMain.Query(Callback_InsertUser, sBuffer, GetClientUserId(iClient), DBPrio_High);
}

public void Callback_InsertUser(Database db, DBResultSet results, const char[] error, int data)
{
	if (db == null) 
	{
		LogError("(InsertUser) - %s", error);
		return;
	}
	
	int iClient = GetClientOfUserId(data);
	
	if(iClient <= 0 || !IsClientInGame(iClient))
	{
		OnClientDisconnect_Post(iClient);
		return;
	}
	
	g_iPoints[iClient] = 0;
	LoadRank(iClient);
}

void LoadRank(int iClient)
{
	char sBuffer[512];
	FormatEx(sBuffer, sizeof(sBuffer), "SELECT COUNT(*) FROM `ranking` WHERE `points` >= %i ORDER BY `points` DESC", g_iPoints[iClient]);
	SQL_TQuery(g_dMain, Callback_LoadRank, sBuffer, GetClientUserId(iClient));
}

public void Callback_LoadRank(Handle hOwner, Handle hHndl, char[] sError, int iUserId) 
{
	if (hHndl == null) 
	{
		LogError("(LoadRank) - %s", sError);
		return;
	}
	
	int iClient;

	iClient = GetClientOfUserId(iUserId);
	
	if(iClient <= 0 || !IsClientInGame(iClient))
	{
		OnClientDisconnect_Post(iClient);
		return;
	}
	
	if(SQL_FetchRow(hHndl))
		g_iPointsRank[iClient] = SQL_FetchInt(hHndl, 0);
	
	Call_StartForward(g_OnRankLoaded);
	Call_PushCell(iClient);
	Call_Finish();
}

void StartTimer()
{
	if(g_hTimer != null)
		delete g_hTimer;
	
	g_hTimer = CreateTimer(30.0, Timer_UpdateTotal, _, TIMER_REPEAT);
}

public Action Timer_UpdateTotal(Handle timer)
{
	GetTotal();
	LoadRankAll();
	GetTopPlayers();
	
	return Plugin_Continue;
}

void LoadRankAll()
{
	char sSteamIDs[512];
	int iCount;
	
	LoopIngamePlayers(iClient)
	{
		if(g_iPoints[iClient] == -1)
			continue;
		
		iCount++;
		
		Format(sSteamIDs, sizeof(sSteamIDs), "%s%s%i", sSteamIDs, iCount == 1 ? "" : ",", g_sAuth[iClient]);
	}
	
	if(iCount == 0)
		return;
	
	char sBuffer[2048];
	FormatEx(sBuffer, sizeof(sBuffer), "SELECT s.steamid, s.place FROM (SELECT steamid, @rownum:=@rownum + 1 AS place FROM ranking JOIN (SELECT @rownum:=0) r ORDER BY points DESC) s WHERE s.steamid IN (%s);", sSteamIDs);
	SQL_TQuery(g_dMain, Callback_LoadRankAll, sBuffer);
}

public void Callback_LoadRankAll(Handle hOwner, Handle hHndl, char[] sError, int data) 
{
	if (hHndl == null) 
	{
		LogError("(LoadRankAll) - %s", sError);
		return;
	}
	
	while (SQL_FetchRow(hHndl))
	{
		char sAuth[18];
		SQL_FetchString(hHndl, 0, sAuth, sizeof(sAuth));
		int iClient = GetClientofSteamID(sAuth);
		
		g_iPointsRank[iClient] = SQL_FetchInt(hHndl, 1);
		
		Call_StartForward(g_OnRankLoaded);
		Call_PushCell(iClient);
		Call_Finish();
	}
}

int GetClientofSteamID(char[] steamid)
{
	LoopIngamePlayers(iClient)
	{
		if(!StrEqual(g_sAuth[iClient], steamid))
			continue;
		
		return iClient;
	}
	
	return -1;
}