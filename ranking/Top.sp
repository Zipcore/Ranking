ArrayList g_aTopPoints = null;
ArrayList g_aTopNames = null;

void InitToplist()
{
	g_aTopPoints = new ArrayList();
	g_aTopNames = new ArrayList(32);
}

public Action Cmd_Top(int iClient, int iArgs)
{
	if(g_aTopPoints.Length < 1)
	{
		CPrintToChat(iClient, "{yellow}[Ranking] {darkred} No players found, please try again later.");
		return Plugin_Handled;
	}
	
	Menu menu = new Menu(Menu_Top_Callback);
	
	char sBuffer[512], sName[32];
	int iPoints;
	LoopArray(i, g_aTopPoints)
	{
		iPoints = g_aTopPoints.Get(i);
		g_aTopNames.GetString(i, sName, sizeof(sName));
		Format(sBuffer, sizeof(sBuffer), "#%i %s (%i)", i+1, sName, iPoints);
		menu.AddItem("", sBuffer);
	}
	
	menu.ExitButton = true;
	menu.Display(iClient, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}

public int Menu_Top_Callback(Menu menu, MenuAction action, int client, int info)
{
	if(action == MenuAction_End)
		delete menu;
}

void GetTopPlayers()
{
	char sBuffer[2048];
	FormatEx(sBuffer, sizeof(sBuffer), "SELECT `name`,`points` FROM `ranking` WHERE 1 ORDER BY `points` DESC LIMIT 10");
	SQL_TQuery(g_dMain, Callback_GetTopPlayers, sBuffer);
}

public void Callback_GetTopPlayers(Handle hOwner, Handle hHndl, char[] sError, int data) 
{
	if (hHndl == null) 
	{
		LogError("(GetTopPlayers) - %s", sError);
		return;
	}
	
	g_aTopPoints.Clear();
	g_aTopNames.Clear();
	
	char sName[32];
	int iPoints;
	while (SQL_FetchRow(hHndl))
	{
		SQL_FetchString(hHndl, 0, sName, sizeof(sName));
		g_aTopNames.PushString(sName);
		
		iPoints = SQL_FetchInt(hHndl, 1);
		g_aTopPoints.Push(iPoints);
	}
}