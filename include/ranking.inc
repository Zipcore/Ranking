forward void Ranking_OnRankLoaded(int iClient);

native int Ranking_GetTotal();
native int Ranking_GetPoints(int iClient);
native int Ranking_GetRank(int iClient);
native bool Ranking_AddPoints(int iClient, int iPoints);
native bool Ranking_TakePoints(int iClient, int iPoints);

stock void Ranking_MarkNativesAsOptional()
{
	MarkNativeAsOptional("Ranking_GetTotal");
	MarkNativeAsOptional("Ranking_GetPoints");
	MarkNativeAsOptional("Ranking_GetRank");
	MarkNativeAsOptional("Ranking_AddPoints");
	MarkNativeAsOptional("Ranking_TakePoints");
}